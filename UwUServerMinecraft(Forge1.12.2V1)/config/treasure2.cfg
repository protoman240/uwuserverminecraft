# Configuration file

general {

    ##########################################################################################################
    # 01-logging
    #--------------------------------------------------------------------------------------------------------#
    # Logging properties
    ##########################################################################################################

    01-logging {
        # The base filename of the  log file.
        S:filename=treasure2

        # The directory where the logs should be stored.
        # This is relative to the Minecraft install path.
        S:folder=mods/treasure2/logs/

        # The logging level. Set to 'off' to disable logging.
        # Values = [trace|debug|info|warn|error|off]
        S:level=debug

        # The size a log file can be before rolling over to a new file.
        S:size=1000K
    }

    ##########################################################################################################
    # 02-mod
    #--------------------------------------------------------------------------------------------------------#
    # General mod properties.
    ##########################################################################################################

    02-mod {
        # Enable/Disable a check to ensure the default loot tables exist on the file system.
        # If enabled, then you will not be able to remove any default loot tables (but they can be edited).
        # Only disable if you know what you're doing.
        B:enableDefaultLootTablesCheck=true

        # Enable/Disable a check to ensure the default templates exist on the file system.
        # If enabled, then you will not be able to remove any default templates.
        # Only disable if you know what you're doing.
        B:enableDefaultTemplatesCheck=true

        # Enable/Disable whether a fog is generated (ex. around graves/tombstones and wither trees)
        B:enableFog=true

        # Enable/Disable whether a Key can break when attempting to unlock a Lock.
        B:enableKeyBreaks=true

        # Enable/Disable whether a Lock item is dropped when unlocked by Key item.
        B:enableLockDrops=true

        # Enable/Disable whether a poison fog is generated (ex. around wither trees)
        B:enablePoisonFog=true

        # Enables/Disables version checking.
        B:enableVersionChecker=true

        # Enable/Disable whether a wither fog is generated (ex. around wither trees)
        B:enableWitherFog=true

        # Enables/Disables mod.
        B:enabled=true

        # The relative path to the mods folder.
        S:folder=mods

        # The latest published version number.
        # This is auto-updated by the version checker.
        # This may be @deprecated.
        S:latestVersion=

        # Remind the user of the latest version (as indicated in latestVersion proeprty) update.
        B:latestVersionReminder=true

        # Where default Treasure folder is located.
        S:treasureFolder=mods/treasure2/

        02-mod-01 {
            # A list of mods that have prebuilt loot tables available.
            # Note: used for informational purposes only.
            S:availableForeignModLootTables <
                mocreatures
                sgs_metals
             >

            # Add mod's MODID to this list to enable custom loot tables for a mod.
            S:enableForeignModIDs <
                mocreatures
                sgs_metals
             >
        }

    }

    ##########################################################################################################
    # 03-chests
    #--------------------------------------------------------------------------------------------------------#
    # Chest properties
    ##########################################################################################################

    03-chests {
        # The number of chests that are monitored. Most recent additions replace least recent when the registry is full.
        # This is the set of chests used to measure distance between newly generated chests.
        # Min: 5
        # Max: 100
        I:chestRegistrySize=25

        # The minimum distance in chunks that can be between any two chests.
        # Note: Only chests in the chest registry are checked against this property.
        # Used in conjunction with the chunks per chest and generation probability.
        # Ex. 
        # Min: 0
        # Max: 32000
        I:minDistancePerChest=75

        ##########################################################################################################
        # 03-chests-01
        #--------------------------------------------------------------------------------------------------------#
        # Common chest properties
        ##########################################################################################################

        03-chests-01 {
            # Disallowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeBlackList <
                plains
                ocean
                deep_ocean
             >

            # Allowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeWhiteList <
             >

            # The number of chunks generated before a chest generation is attempted.
            # Min: 50
            # Max: 32000
            I:chunksPerChest=75

            # The probability that a chest will generate.
            # Min: 0.0
            # Max: 100.0
            D:genProbability=85.0

            # The probability that a chest will be a mimic.
            # Min: 0.0
            # Max: 100.0
            D:mimicProbability=20.0

            # The minimum depth (y-axis) that a chest can generate at.
            # Min: 5
            # Max: 250
            I:minYSpawn=50
        }

        ##########################################################################################################
        # 03-chests-02
        #--------------------------------------------------------------------------------------------------------#
        # Uncommon chest properties
        ##########################################################################################################

        03-chests-02 {
            # Disallowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeBlackList <
             >

            # Allowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeWhiteList <
             >

            # The number of chunks generated before a chest generation is attempted.
            # Min: 50
            # Max: 32000
            I:chunksPerChest=150

            # The probability that a chest will generate.
            # Min: 0.0
            # Max: 100.0
            D:genProbability=75.0

            # The probability that a chest will be a mimic.
            # Min: 0.0
            # Max: 100.0
            D:mimicProbability=0.0

            # The minimum depth (y-axis) that a chest can generate at.
            # Min: 5
            # Max: 250
            I:minYSpawn=40
        }

        ##########################################################################################################
        # 03-chests-03
        #--------------------------------------------------------------------------------------------------------#
        # Scarce chest properties
        ##########################################################################################################

        03-chests-03 {
            # Disallowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeBlackList <
             >

            # Allowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeWhiteList <
             >

            # The number of chunks generated before a chest generation is attempted.
            # Min: 50
            # Max: 32000
            I:chunksPerChest=300

            # The probability that a chest will generate.
            # Min: 0.0
            # Max: 100.0
            D:genProbability=50.0

            # The probability that a chest will be a mimic.
            # Min: 0.0
            # Max: 100.0
            D:mimicProbability=0.0

            # The minimum depth (y-axis) that a chest can generate at.
            # Min: 5
            # Max: 250
            I:minYSpawn=30
        }

        ##########################################################################################################
        # 03-chests-04
        #--------------------------------------------------------------------------------------------------------#
        # Rare chest properties
        ##########################################################################################################

        03-chests-04 {
            # Disallowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeBlackList <
             >

            # Allowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeWhiteList <
             >

            # The number of chunks generated before a chest generation is attempted.
            # Min: 50
            # Max: 32000
            I:chunksPerChest=500

            # The probability that a chest will generate.
            # Min: 0.0
            # Max: 100.0
            D:genProbability=25.0

            # The probability that a chest will be a mimic.
            # Min: 0.0
            # Max: 100.0
            D:mimicProbability=0.0

            # The minimum depth (y-axis) that a chest can generate at.
            # Min: 5
            # Max: 250
            I:minYSpawn=20
        }

        ##########################################################################################################
        # 03-chests-05
        #--------------------------------------------------------------------------------------------------------#
        # Epic chest properties
        ##########################################################################################################

        03-chests-05 {
            # Disallowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeBlackList <
             >

            # Allowable Biome Types for general Chest generation. Must match the Type identifer(s).
            S:biomeWhiteList <
             >

            # The number of chunks generated before a chest generation is attempted.
            # Min: 50
            # Max: 32000
            I:chunksPerChest=800

            # The probability that a chest will generate.
            # Min: 0.0
            # Max: 100.0
            D:genProbability=15.0

            # The probability that a chest will be a mimic.
            # Min: 0.0
            # Max: 100.0
            D:mimicProbability=0.0

            # The minimum depth (y-axis) that a chest can generate at.
            # Min: 5
            # Max: 250
            I:minYSpawn=10
        }

    }

    ##########################################################################################################
    # 04-well
    #--------------------------------------------------------------------------------------------------------#
    # Well properties
    ##########################################################################################################

    04-well {
        # Disallowable Biome Types for general Well generation. Must match the Type identifer(s).
        S:biomeBlackList <
            ocean
            deep_ocean
         >

        # Allowable Biome Types for general Well generation. Must match the Type identifer(s).
        S:biomeWhiteList <
         >

        # The number of chunks generated before a well generation is attempted.
        # Min: 50
        # Max: 32000
        I:chunksPerWell=500

        # The probability that a well will generate.
        # Min: 0.0
        # Max: 100.0
        D:genProbability=80.0

        # Toggle to allow/disallow the generation of well.
        B:wellAllowed=true
    }

    ##########################################################################################################
    # 05-wither tree
    #--------------------------------------------------------------------------------------------------------#
    # Wither tree properties
    ##########################################################################################################

    "05-wither tree" {
        # Disallowable Biome Types for Wither Tree generation. Must match the Type identifer(s).
        S:biomeBlackList <
         >

        # Allowable Biome Types for Wither Tree generation. Must match the Type identifer(s).
        S:biomeWhiteList <
            forest
            magical
            lush
            spooky
            dead
            jungle
            coniferous
            savanna
         >

        # The number of chunks generated before a wither tree generation is attempted.
        # Min: 200
        # Max: 32000
        I:chunksPerTree=800

        # The probability that a wither tree will generate.
        # Min: 0.0
        # Max: 100.0
        D:genProbability=90.0

        # 
        # Min: 0
        # Max: 30
        I:maxSupportingTrees=15

        # 
        # Min: 7
        # Max: 20
        I:maxTrunkSize=0

        # 
        # Min: 0
        # Max: 30
        I:minSupportingTrees=5
    }

}


