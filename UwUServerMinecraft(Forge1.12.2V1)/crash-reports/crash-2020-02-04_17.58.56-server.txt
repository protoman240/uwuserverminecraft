---- Minecraft Crash Report ----

WARNING: coremods are present:
  llibrary (llibrary-core-1.0.11-1.12.2.jar)
  midnight (themidnight-0.3.5.jar)
  MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.244.jar)
  McLib core mod (mclib-1.0.3-1.12.2.jar)
  CTMCorePlugin (CTM-MC1.12.2-0.3.3.22.jar)
Contact their authors BEFORE contacting forge

// Shall we play a game?

Time: 4/02/20 17:58
Description: Exception in server tick loop

Missing Mods:
	unknown : need [1.0.11,): have missing

net.minecraftforge.fml.common.MissingModsException: Mod netherex (NetherEx) requires [libraryex@[1.0.11,)]
	at net.minecraftforge.fml.common.Loader.sortModList(Loader.java:266)
	at net.minecraftforge.fml.common.Loader.loadMods(Loader.java:572)
	at net.minecraftforge.fml.server.FMLServerHandler.beginServerLoading(FMLServerHandler.java:98)
	at net.minecraftforge.fml.common.FMLCommonHandler.onServerStart(FMLCommonHandler.java:333)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:125)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:486)
	at java.lang.Thread.run(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_221, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 3205479696 bytes (3056 MB) / 4116185088 bytes (3925 MB) up to 9544663040 bytes (9102 MB)
	JVM Flags: 2 total; -Xms4G -Xmx10G
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2847 58 mods loaded, 57 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State | ID                   | Version                  | Source                                           | Signature |
	|:----- |:-------------------- |:------------------------ |:------------------------------------------------ |:--------- |
	| L     | minecraft            | 1.12.2                   | minecraft.jar                                    | None      |
	| L     | mcp                  | 9.42                     | minecraft.jar                                    | None      |
	| L     | FML                  | 8.0.99.99                | forge-1.12.2-14.23.5.2847-universal.jar          | None      |
	| L     | forge                | 14.23.5.2847             | forge-1.12.2-14.23.5.2847-universal.jar          | None      |
	| L     | micdoodlecore        |                          | minecraft.jar                                    | None      |
	| UD    | mclib_core           | 1.0.3                    | minecraft.jar                                    | None      |
	| L     | deconstruction       | 3.0.4                    | [MC1.12.2]DeconTable-3.0.4.jar                   | None      |
	| L     | baubles              | 1.5.2                    | Baubles-1.12-1.5.2.jar                           | None      |
	| L     | betternether         | 0.1.8.3                  | betternether-0.1.8.3.jar                         | None      |
	| L     | biomesoplenty        | 7.0.1.2441               | BiomesOPlenty-1.12.2-7.0.1.2441-universal.jar    | None      |
	| L     | bookshelf            | 2.3.585                  | Bookshelf-1.12.2-2.3.585.jar                     | None      |
	| L     | bookworm             | 1.12.2-2.2.0             | bookworm+-+2.2.0+(1.12.2).jar                    | None      |
	| L     | chesttransporter     | 2.8.8                    | ChestTransporter-1.12.2-2.8.8.jar                | None      |
	| L     | conarm               | 1.2.4                    | conarm-1.12.2-1.2.4.jar                          | None      |
	| L     | d3core               | 1.3.2.71                 | D3Core-1.12.2-1.3.2.71.jar                       | None      |
	| L     | darkutils            | 1.8.230                  | DarkUtils-1.12.2-1.8.230.jar                     | None      |
	| L     | dimdoors             | 3.0.9+287                | DimensionalDoors-3.0.9-287.jar                   | None      |
	| L     | emoticons            | 0.3.1                    | emoticons-0.3.1-1.12.2-dist.jar                  | None      |
	| L     | eplus                | 5.0.176                  | EnchantingPlus-1.12.2-5.0.176.jar                | None      |
	| L     | endreborn            | 0.3.1                    | EndReborn+III+[0.3.1].jar                        | None      |
	| L     | extrautils2          | 1.0                      | extrautils2-1.12-1.9.9.jar                       | None      |
	| L     | galacticraftplanets  | 4.0.2.244                | Galacticraft-Planets-1.12.2-4.0.2.244.jar        | None      |
	| L     | galacticraftcore     | 4.0.2.244                | GalacticraftCore-1.12.2-4.0.2.244.jar            | None      |
	| L     | gottschcore          | 1.8.0                    | GottschCore-mc1.12.2-f14.23.5.2768-v1.8.0.jar    | None      |
	| L     | heroicarmory         | 1.1.3                    | heroicarmory-1.2.0.jar                           | None      |
	| L     | huntingdim           | 1.0.42                   | HuntingDimension-1.12.2-1.0.42.jar               | None      |
	| L     | inventorypets        | 2.0.3                    | inventorypets-1.12-2.0.3.jar                     | None      |
	| L     | ironchest            | 1.12.2-7.0.67.844        | ironchest-1.12.2-7.0.72.847.jar                  | None      |
	| L     | ironfurnaces         | 1.3.5                    | ironfurnaces-1.3.5.jar                           | None      |
	| L     | locks                | 2.2.1                    | Locks-2.2.1.jar                                  | None      |
	| L     | lumberjack           | 1.4.1                    | lumberjack-1.4.1.jar                             | None      |
	| L     | mantle               | 1.12-1.3.3.55            | Mantle-1.12-1.3.3.55.jar                         | None      |
	| L     | mclib                | 1.0.3                    | mclib-1.0.3-1.12.2.jar                           | None      |
	| L     | mowziesmobs          | 1.5.4                    | mowziesmobs-1.5.4.jar                            | None      |
	| L     | naturesaura          | r18                      | NaturesAura-r18.jar                              | None      |
	| L     | netherex             | 2.0.15                   | NetherEx-1.12.2-2.0.15.jar                       | None      |
	| L     | patchouli            | 1.0-20                   | Patchouli-1.0-20.jar                             | None      |
	| L     | portality            | 1.0-SNAPSHOT             | portality-1.12.2-1.2.3-15.jar                    | None      |
	| L     | reborncore           | 3.16.0.469               | RebornCore-1.12.2-3.16.0.469-universal.jar       | None      |
	| L     | additionalstructures | 2.3.1                    | Rex's-Additional-Structures-1.12.x-(v.2.3.1).jar | None      |
	| L     | spartanshields       | 1.5.4                    | SpartanShields-1.12.2-1.5.4.jar                  | None      |
	| L     | stevescarts          | 2.4.31.135               | StevesCarts-1.12.2-2.4.31.135.jar                | None      |
	| L     | tconstruct           | 1.12.2-2.12.0.157        | TConstruct-1.12.2-2.12.0.157.jar                 | None      |
	| L     | thaumcraft           | 6.1.BETA26               | Thaumcraft-1.12.2-6.1.BETA26.jar                 | None      |
	| L     | tinkersjei           | 1.2                      | tinkersjei-1.2.jar                               | None      |
	| L     | tinkertoolleveling   | 1.12.2-1.1.0.DEV.b23e769 | TinkerToolLeveling-1.12.2-1.1.0.jar              | None      |
	| L     | toolbelt             | 1.9.12                   | ToolBelt-1.12.2-1.9.12.jar                       | None      |
	| L     | twilightforest       | 3.10.1013                | twilightforest-1.12.2-3.10.1013-universal.jar    | None      |
	| L     | uteamcore            | 2.2.4.107                | u_team_core-1.12.2-2.2.4.107.jar                 | None      |
	| L     | uppers               | 0.0.6                    | Uppers-0.0.6.jar                                 | None      |
	| L     | usefulbackpacks      | 1.5.1.35                 | useful_backpacks-1.12.2-1.5.1.35.jar             | None      |
	| L     | vanillafoodpantry    | 4.3.1                    | vanillafoodpantry-mc1.12.2-4.3.1.jar             | None      |
	| L     | warpbook             | @VERSION@                | Warpbook-1.12.2-3.2.0.jar                        | None      |
	| L     | waystones            | 4.1.0                    | Waystones_1.12.2-4.1.0.jar                       | None      |
	| L     | xlfoodmod            | 1.12.2-1.9.2             | XL-Food-Mod-1.12.2-1.9.2.jar                     | None      |
	| L     | xpholder             | 2.0.4                    | XpHolder-2.0.4-mc1.12.x.jar                      | None      |
	| L     | llibrary             | 1.7.19                   | llibrary-1.7.19-1.12.2.jar                       | None      |
	| L     | midnight             | 0.3.5                    | themidnight-0.3.5.jar                            | None      |

	Loaded coremods (and transformers): 
llibrary (llibrary-core-1.0.11-1.12.2.jar)
  net.ilexiconn.llibrary.server.core.plugin.LLibraryTransformer
  net.ilexiconn.llibrary.server.core.patcher.LLibraryRuntimePatcher
midnight (themidnight-0.3.5.jar)
  com.mushroom.midnight.core.transformer.MidnightClassTransformer
MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.244.jar)
  micdoodle8.mods.miccore.MicdoodleTransformer
McLib core mod (mclib-1.0.3-1.12.2.jar)
  mchorse.mclib.core.McLibCMClassTransformer
CTMCorePlugin (CTM-MC1.12.2-0.3.3.22.jar)
  team.chisel.ctm.client.asm.CTMTransformer
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'fml,forge'
	Type: Dedicated Server (map_server.txt)